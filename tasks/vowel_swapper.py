def vowel_swapper(input_string):
    new_string = ""
    for word in input_string:
        if word == "O":
            new_string +="000"
            continue
        if word in "aA":
            new_string +="4"
        elif word in "eE":
            new_string +="3"
        elif word in "iI":
            new_string +="!"
        elif word in "o":
            new_string +="ooo"
        elif word in "uU":
            new_string += "|_|"
        else: 
            new_string += word

    return new_string
print(vowel_swapper("aA eE iI oO uU"))
print(vowel_swapper("Hello World"))
print(vowel_swapper("Everything's Available"))
