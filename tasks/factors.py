def factors(num):
    new_list = []
    for i in range(2,num):
        if num%i == 0:
            new_list.append(i)
    if new_list:
        return new_list
    else:
        return f"{num} is a prime number"

print(factors(15))  
print(factors(12))
print(factors(13))
