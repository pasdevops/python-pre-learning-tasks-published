def bin_number(i):
    return "{0:b}".format(i)

def calculator(a, b, operator):
    if operator == "+":
        return bin_number(a+b) 
    elif operator == "-":
        return bin_number(a-b)
    elif operator == "*":
        return bin_number(a*b)
    elif operator == "/":
        return bin_number(int(a/b))

print(calculator(2, 4, "+"))
print(calculator(10, 3, "-"))
print(calculator(4, 7, "*"))
print(calculator(100, 2, "/"))
